#ifndef STRUCTS_H
#define STRUCTS_H

/**************************
Areas
**************************/

typedef struct AreaLinkedList{
	int areaCode;
	struct Area *nextArea;
	char *areaName;
}*Area;

void printArea(Area);

int countArea(Area);

/**************************
Phone Number
**************************/

typedef struct PhEntryLinkedList{
	int areaCode;
	int phoneNum;
	struct PhEntry *nextPhEntry;
	char *firstName;
	char *lastName;
}*PhEntry;

void printPh(PhEntry);

int countPh(PhEntry);


/**************************
Functions
**************************/

Area addToArea(Area);

PhEntry addToPh(PhEntry);

void modifyPh(void);

void deletePh(PhEntry);

#endif