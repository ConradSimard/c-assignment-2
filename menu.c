/* PROGRAM: Assignment 2
   AUTHOR:  Conrad Simard
   DATE:   2018/11/10
   PURPOSE: structs and linked list
   LEVEL OF DIFFICULTY: Hard
   CHALLENGES: core dumpped every time I try to iterate throguh the list
   HOURS SPENT: more then id like to admit
*/

/**************************
File Menu.c
**************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "menu.h"
#include "functions.h"

/*loop for menu options*/
void menu(Area headArea, PhEntry headPhEntry){
	char *input;
	
	/*headArea = fillArea(headArea);*/
	/*headPhEntry = fillPhEntry(headPhEntry);*/
	
	do
	{
		displayMenu(); /*displays menu*/
		scanf("%s", &input);
		
		switch((char)input)
		{
			case '1': 	headArea = addToArea(headArea);
				break;
			case '2':	headPhEntry = addToPhEntry(headPhEntry);
				break;
			case '3':	
				break;
			case '4':	headPhEntry = deletePh(headPhEntry);	
				break;
			case '5':	printf("There are [%d] Areas and [%d] Phone Entries\n", countArea(headArea), countPhEntry(headPhEntry));
				break;
			case 'Q':	break;
			default:	printf("Incorrect option\n");
		}

	}while(input != 'Q');

}

/*prints a graphic line*/
void printLine(void)
{
	printf("------------------------------\n");
}

/*prints a header*/
void printHeader(char *string)
{
	printf("\n");
	printf("  %s\n", string);
	printLine();
}

/*prints the menu*/
void displayMenu(void)
{
	printHeader("Main Menu");
	printf("[1]: Enter Area Information\n");
	printf("[2]: Enter PhoneBook Entry\n");
	printf("[3]: Moditfy existing PhonBook Entry\n");
	printf("[4]: Delete existing PhoneBook Entry\n");
	printf("[5]: Display Area and Phone Number Totals\n");
	printf("[Q]: Quite\n");
	printf("-> ");
}