/* PROGRAM: Assignment 2
   AUTHOR:  Conrad Simard
   DATE:   2018/11/10
   PURPOSE: structs and linked list
   LEVEL OF DIFFICULTY: Hard
   CHALLENGES: core dumpped every time I try to iterate throguh the list
   HOURS SPENT: more then id like to admit
*/

/**************************
File Functions.c
**************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "menu.h"
#include "functions.h"

/**************************
AreaList
**************************/

/*creats a blank node*/
Area * createNodeArea()
{
	Area newArea = (Area *)malloc(sizeof(Area)+1);
	newArea->nextArea = NULL;
	return newArea;
}

/*appends an area code entry to the linked list*/
Area * apendArea(int areaCode, char* areaName, Area head)
{
	Area newArea = createNodeArea();
	newArea->areaCode = areaCode;
	newArea->areaName = areaName;
	newArea->nextArea = (Area *)malloc(sizeof(Area)+1);
	newArea->nextArea = head;	
	return newArea;
}

/*iterates throguh the linked list and prints data*/
void printArea(Area head)
{
	Area current = head;
	int count = 0;
	
	printf("There are [%d] Areas\n", countArea(head));
	while (current != NULL)
	{
		printf("[%d] %d\t%s\n", count, current->areaCode, current->areaName);
		count++;
		current = current->nextArea;
	}
}

/*counts the number of linked items and returns the total*/
int countArea(Area head)
{
	int count;
	Area current = head;
	
	while (current != NULL)
	{
		count++;
		current = current->nextArea;
	}
	
	return count;
}

/**************************
PhList
**************************/
/*creates a blank node*/
PhEntry * createNodePhEntry()
{
	PhEntry newPh = (PhEntry *)malloc(sizeof(PhEntry)+1);
	newPh->nextPhEntry = NULL;
	return newPh;
}

/*preppends a phone entry to the linked list*/
PhEntry * prependPhEntry(int areaCode, int number, char *firstName, char *lastName, PhEntry head)
{
	PhEntry newPh = createNodePhEntry();
	newPh->areaCode = areaCode;
	newPh->phoneNum = number;
	newPh->firstName = firstName;
	newPh->lastName = lastName;
	newPh->nextPhEntry = (PhEntry *)malloc(sizeof(PhEntry)+1);
	newPh->nextPhEntry = head;	
	return newPh;
}

/*iterates throguh the linked list and prints data*/
void printPhEntry(PhEntry head)
{
	PhEntry current = head;
	int count = 0;
	
	printf("There are [%d] phone entries\n", countPhEntry(head));
	while (current != NULL)
	{
		printf("[%d] %d\t%d\t%s, %s\n", count, current->areaCode, current->phoneNum, current->lastName, current->firstName);
		count++;
		current = current->nextPhEntry;
	}
}

/*counts the number of linked items and returns the total*/
int countPhEntry(PhEntry head)
{
	int count;
	PhEntry current = head;
	
	while (current != NULL)
	{
		count++;
		current = current->nextPhEntry;
	}
	
	return count;
}
/**************************
Functions
**************************/

/*prefills the linked list with data*/
Area fillArea(Area headArea){
	printHeader("Areas");
	printf("Empty\n");
	
	printHeader("Areas");
	headArea = apendArea(613, "Ottawa", headArea);
	printArea(headArea);
	printHeader("Areas");
	headArea = apendArea(416, "Toronto", headArea);
	printArea(headArea);
	printHeader("Areas");
	headArea = apendArea(647, "Toronto", headArea);
	printArea(headArea);
	printHeader("Areas");
	headArea = apendArea(519, "Windsor", headArea);
	printArea(headArea);
	printHeader("Areas");
	headArea = apendArea(905, "Niagra", headArea);
	printArea(headArea);
	return headArea;
}

/*prefills the linked list with data*/
Area fillPhEntry(PhEntry headPhEntry){
	printHeader("Areas");
	printf("Empty\n");
	
	headPhEntry = prependPhEntry(613, 2222222, "Bob", "Ross", headPhEntry);
	headPhEntry = prependPhEntry(514, 2222222, "Jane", "Doe", headPhEntry);
	headPhEntry = prependPhEntry(819, 2222222, "Josh", "Butch", headPhEntry);
	headPhEntry = prependPhEntry(406, 2222222, "Betty", "Who", headPhEntry);

	printHeader("Entries");
	printPhEntry(headPhEntry);
	
	return headPhEntry;
}

/*asks user for data and then adds data to the linked list*/
Area addToArea(Area headArea)
{
	int area, pass;
	char *name;
	
	printHeader("Existing Areas");
	if(headArea == NULL)
	{
		printf("Empty\n");
	}else{
		printArea(headArea);
	}
	printHeader("Add Area");
	
	do{
		pass=1;
		printf("Enter area code: ");
		if(!scanf("%d", &area)) {
			printf("not a number\n");
			while(getchar() != '\n') continue;
			pass=0;
		}
	}while((area > 1000) || (area < 99));
	
	printf("Enter name of area: ");
	scanf("%s", &name);
	while(getchar() != '\n') continue;
	
	headArea = apendArea(area, &name, headArea);
	/*headArea = apendArea(613, "Ottawa", headArea);*/
	printHeader("Areas");
	printArea(headArea);
	return headArea;

}

/*asks user for data and then adds data to the linked list*/
PhEntry addToPhEntry(PhEntry headPhEntry)
{
	int area, number;
	char *first, *last;
	
	printHeader("Existing Phone Entries");
	if(headPhEntry == NULL)
	{
		printf("Empty\n");
	}else{
		printPhEntry(headPhEntry);
	}
	printHeader("Add Phone Entry");
	
	/*user input for area code*/
	do{
		printf("Enter area code: ");
		if(!scanf("%d", &area)) {
			printf("not a number\n");
			while(getchar() != '\n') continue;
		}
	}while((area > 999) || (area < 100));
	
	/*user input for phone number*/
	do{
		printf("Enter phone number: ");
		if(!scanf("%d", &area)) {
			printf("not a number\n");
			while(getchar() != '\n') continue;
		}
	}while((number > 99999999) || (number < 10000000));
	
	/*user input for first name*/
	printf("Enter first name: ");
	scanf("%s", &first);
	/*user input for last name*/
	printf("Enter last name: ");
	scanf("%s", &last);
	
	headPhEntry = prependPhEntry(area, number, &first, &last, headPhEntry);
	/*headPhEntry = prependPhEntry(613, 2222222, "Bob", "Ross", headPhEntry);*/
	printHeader("Phone Entries");
	printPhEntry(headPhEntry);
	return headPhEntry;

}

/*asks user for search key, searches for index, then modifys it*/
void modifyPh()
{
	
	
}

/*asks user for search key, searches for index, then deletes it*/
void deletePh(PhEntry head)
{
	if(head == NULL){
		printf("No phone entries");
		return;
	}

	char *name;
	int count, i;
	PhEntry current = head;
	PhEntry previous = createNodePhEntry();
	printf("Enter last name: ");
	scanf("%s", &name);
	
	while (current != NULL)
	{
		if(current->lastName == name) break;
		count++;
		current = current->nextPhEntry;
	}
	
	for(i=0; i<count; i++) previous = previous->nextPhEntry;
	
	previous = current;
	current = current->nextPhEntry;
}
